package models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Transaction {

    private String date;
    private String description;
    private String deposit;
    private String withdrawal;

    @Override
    public String toString() {
        return "Transaction{" +
                "date='" + date + '\'' +
                ", description='" + description + '\'' +
                ", deposit='" + deposit + '\'' +
                ", withdrawal='" + withdrawal + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (!date.equals(that.date)) return false;
        if (!description.equals(that.description)) return false;
        if (deposit != null ? !deposit.equals(that.deposit) : that.deposit != null) return false;
        return withdrawal != null ? withdrawal.equals(that.withdrawal) : that.withdrawal == null;
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + (deposit != null ? deposit.hashCode() : 0);
        result = 31 * result + (withdrawal != null ? withdrawal.hashCode() : 0);
        return result;
    }
}
