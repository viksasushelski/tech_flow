package pages;

import Log.LogUI;
import components.ShowTransactions;
import data.enums.AccountType;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import utils.base.BasePage;

@Getter
public class AccountActivityPage extends BasePage {

    @FindBy(id = "aa_accountId")
    private WebElement accountsDDL;

    @FindBy(id = "all_transactions_for_account")
    private WebElement showTranscationsTableContainer;

    //composition of table component templates from ui-commons module
    private ShowTransactions showTransactions;

    public AccountActivityPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new AccountActivityPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindIndicatorElementFromRoot(By.id("aa_accountId"));
    }

    @Step("User has selected account type with value {0}")
    public AccountActivityPage chooseAccount(AccountType accountType) {
        new Select(accountsDDL).selectByVisibleText(accountType.toString());
        LogUI.stepsToReproduce("User has selected accountType: " + accountType.toString());
        waitForElementToBeRemovedAndShown(By.id("all_transactions_for_account"), 1, 1);
        return new AccountActivityPage(getDriver());
    }

    public AccountActivityPage readTransactionTable() {
        showTransactions = new ShowTransactions(getDriver(), showTranscationsTableContainer);
        showTransactions.fillListOfEntires();
        return this;
    }
}
