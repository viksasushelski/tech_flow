package pages;

import Log.LogUI;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.base.BasePage;

public class LoginPage extends BasePage {

    @FindBy(id = "user_login")
    private WebElement inputUserName;

    @FindBy(id = "user_password")
    private WebElement inputPassword;

    @FindBy(className = "btn-primary")
    private WebElement btnSubmit;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new LoginPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindIndicatorElementFromRoot(By.id("user_login"));
    }

    @Step("User has logged in")
    public AccountSummaryPage loginWithCredentials(String userName, String password){
        //all functions are coming from ui-commons module
        clearAndSendKeys(inputUserName, userName);
        clearAndSendKeys(inputPassword, password);
        waitForElementToBeClickableAndClick(btnSubmit);
        LogUI.stepsToReproduce("User has logged in with credentials: [" + userName + "|" + password + "]");
        return new AccountSummaryPage(getDriver());
    }
}
