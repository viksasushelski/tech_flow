package pages;

import Log.LogUI;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.base.BasePage;

public class AccountSummaryPage extends BasePage {

    @FindBy(id = "account_activity_tab")
    private WebElement accountActivityTab;

    public AccountSummaryPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new AccountSummaryPage(driver);
    }

    @Override
    public WebElement getPageIsLoadedIndicatorElement() {
        return waitAndFindIndicatorElementFromRoot(By.id("account_activity_tab"));
    }

    @Step("User has navigated to Account Activity Page")
    public AccountActivityPage navigateToAccountActivityPage() {
        waitForElementToBeClickableAndClick(accountActivityTab);
        LogUI.stepsToReproduce("User has navigated to Account Activity Page");
        return new AccountActivityPage(getDriver());
    }
}
