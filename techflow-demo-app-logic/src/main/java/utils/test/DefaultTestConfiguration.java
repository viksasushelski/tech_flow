package utils.test;

import Enums.DriverType;
import TestUtil.TestConfigurationUIDTO;
import Utils.PropertiesConfigurationDTO;
import data.constants.TestResourcePaths;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static data.enums.ApplicationProperties.*;


public class DefaultTestConfiguration {

    public static final TestConfigurationUIDTO STANDARD_TEST_CONFIGURATION = TestConfigurationUIDTO
            .uiTestConfBuilder()
            .driverType(DriverType.parse(DRIVER_TYPE.getConfigurationValue()))
            .driverVersion(DRIVER_VERSION_PROPERTY_NAME.getConfigurationValue())
            .driverHeadless(Boolean.valueOf(HEADLESS_PROPERTY_NAME.getConfigurationValue()))
            .baseUrl(BASE_URL_PROPERTY_NAME.getConfigurationValue())
            .allowedUrls(new HashSet(Collections.singletonList("http://zero.webappsecurity.com")))
            .initialFailedTestScreenShotPath(TestResourcePaths.SUREFIRE_REPORTS_FOLDER_PATH)
            .reRunFailedTestScreenShotPath(TestResourcePaths.TEST_OUTPUT_FOLDER_PATH)
            .enableConsoleLog(Boolean.valueOf(ENABLE_CONSOLE_LOG.getConfigurationValue()))
            .stepsToReproduceFilePath(TestResourcePaths.STEPS_TO_REPRODUCE_PATH)
            .stepsToReproduceConsoleLogFilePath(TestResourcePaths.CONSOLE_LOG_PATH)
            .disableReRunSuiteNames(new HashSet<String>())
            .includeDbTesting(Boolean.valueOf(INCLUDE_DATABASE_TESTING.getConfigurationValue()))
            .firstSuiteNameInRegression("")
            .numberOfThreads(Integer.valueOf(THREAD_POOL_SIZE_PROPERTY_NAME.getConfigurationValue()))
            .numberOfReRunFailedTests(Integer.valueOf(RERUN_NUMBER_OF_TIMES.getConfigurationValue()))
            .build();

    public static final PropertiesConfigurationDTO STANDARD_PROPERTIES_CONFIGURATION = PropertiesConfigurationDTO
            .builder()
            .propertiesFilePath(TEST_CONFIGURATION_PROPERTIES_FILE_PATH.toString())
            .resultPropertiesPath(RESULTS_PROPERTIES_FILE_PATH.toString())
            .resultTemplatePropertiesPath(RESULTS_TEMPLATE_PROPERTIES_FILE_PATH.toString())
            .allureEnvironmentTemplatePropertiesPath(ENVIRONMENT_TEMPLATE_PROPERTIES_FILE_PATH.toString())
            .allureEnvironmentPropertiesPath(ENVIRONMENT_PROPERTIES_FILE_PATH.toString())
            .baseUrlPropertyName(BASE_URL_PROPERTY_NAME.toString())
            .environmentPropertyName(ENVIRONMENT_PROPERTY_NAME.toString())
            .numberOfThreadsPerSuitePropertyName(THREAD_POOL_SIZE_PROPERTY_NAME.toString())
            .headlessPropertyName(HEADLESS_PROPERTY_NAME.toString())
            .driverVersionPropertyName(DRIVER_VERSION_PROPERTY_NAME.toString())
            .reRunFailedTestsCountPropertyName(RERUN_NUMBER_OF_TIMES.toString())
            .passedTestMethodsPercentagePropertyName(PASSED_TESTS_METHODS_PERCENTAGE.toString())
            .passedTestClassesPercentagePropertyName(PASSED_TESTS_CLASSES_PERCENTAGE.toString())
            .passedTestsPropertyName(PASSED_TESTS.toString())
            .executionPassedTestsMethodsPropertyName(EXECUTION_PASSED_TESTS_METHODS_PERCENTAGE.toString())
            .executionPassedTestsClassesPropertyName(EXECUTION_PASSED_TESTS_CLASSES_PERCENTAGE.toString())
            .includeDatabasePropertyName(INCLUDE_DATABASE_TESTING.toString())
            .databaseQueriesThresholdPropertyName(DATABASE_QUERIES_THRESHOLD.toString())
            .build();

}
