package utils.base;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import utils.selenium.ApplicationSeleniumFunctions;
import utils.selenium.SeleniumSetup;

@Getter
public class BasePageComponent extends SeleniumSetup {

    private ApplicationSeleniumFunctions applicationSeleniumFunctions;

    public BasePageComponent(WebDriver driver) {
        super(driver);
        applicationSeleniumFunctions = new ApplicationSeleniumFunctions(this);
    }

}
