package utils.base;

import TestUtil.TestConfigurationUI;
import lombok.Getter;
import org.testng.ITestContext;
import utils.test.DefaultTestConfiguration;

import static data.enums.ApplicationProperties.ENVIRONMENT_PROPERTY_NAME;


@Getter
public abstract class BaseTest extends TestConfigurationUI {

    public BaseTest() {
        super(DefaultTestConfiguration.STANDARD_TEST_CONFIGURATION, DefaultTestConfiguration.STANDARD_PROPERTIES_CONFIGURATION);
    }

    @Override
    protected void initializeTest(ITestContext testContext) {
        environment = ENVIRONMENT_PROPERTY_NAME.getConfigurationValue();
    }

}
