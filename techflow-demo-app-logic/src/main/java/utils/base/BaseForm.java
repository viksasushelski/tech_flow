package utils.base;

import Log.LogUI;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.selenium.ApplicationSeleniumFunctions;
import utils.selenium.SeleniumSetup;

@Getter
public abstract class BaseForm extends SeleniumSetup {

    private ApplicationSeleniumFunctions applicationSeleniumFunctions;

    public BaseForm(WebDriver driver) {
        super(driver);
        try {
            getPageIsLoadedIndicatorElement();
        } catch (Exception e) {
            LogUI.error("The indicator elements wasn't found for page: " + this.getClass().getName());
        }
        applicationSeleniumFunctions = new ApplicationSeleniumFunctions(this);
    }

    protected abstract WebElement getPageIsLoadedIndicatorElement();

}
