package utils.base;

import Log.LogUI;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.selenium.ApplicationSeleniumFunctions;
import utils.selenium.SeleniumSetup;

import static data.enums.ApplicationProperties.ENABLE_CONSOLE_LOG;

@Getter
public abstract class BasePage extends SeleniumSetup {
    public static final Boolean enableConsoleLog = Boolean.valueOf(ENABLE_CONSOLE_LOG.getConfigurationValue());
    private ApplicationSeleniumFunctions applicationSeleniumFunctions;

    protected Logger log = LoggerFactory.getLogger(getClass());

    public BasePage(WebDriver driver) {
        super(driver);
        applicationSeleniumFunctions = new ApplicationSeleniumFunctions(this);
        try {
            getPageIsLoadedIndicatorElement();
        } catch (TimeoutException | StaleElementReferenceException | NoSuchElementException e) {
            LogUI.error("The indicator elements wasn't found for: " + this.getClass().toString());
        } catch (Exception e) {
            LogUI.error("Problem occurred with indicator element");
        }
        if (enableConsoleLog) {
            LogEntries browser = driver.manage().logs().get("browser");
            for (LogEntry logEntry : browser.getAll()) {
                LogUI.writeConsoleLog(logEntry.getMessage());
            }
        }
    }

    public abstract BasePage newInstance(WebDriver driver);

    public abstract WebElement getPageIsLoadedIndicatorElement();


    @Step("User has navigated to url: {0}")
    public BasePage navigateTo(String url) {
        executeCurrentMethodLog();
        getDriver().get(url);
        LogUI.stepsToReproduce("User has navigated to: " + url);
        return newInstance(getDriver());
    }

}
