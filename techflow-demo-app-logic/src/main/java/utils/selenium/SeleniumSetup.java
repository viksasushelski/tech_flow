package utils.selenium;

import Log.LogUI;
import UiFunctions.Configuration.SeleniumConfiguration;
import UiFunctions.SeleniumFunctions;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class SeleniumSetup extends SeleniumFunctions {
    private final static int MAX_DEFAULT_WAIT_TIME = 30;
    private final static int MAX_INDICATOR_WAIT_TIME = 2;
    private final static int MAX_LOCATING_WAIT_TIME = 10;

    public SeleniumSetup(WebDriver driver) {
        super(SeleniumConfiguration.builder()
                .driver(driver)
                .defaultWaitingTime(MAX_DEFAULT_WAIT_TIME)
                .waitLocating(MAX_LOCATING_WAIT_TIME)
                .indicatorWait(MAX_INDICATOR_WAIT_TIME)
                .build());
    }

    protected void executeCurrentMethodLog() {
        String className = Thread.currentThread().getStackTrace()[2].getClassName();
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        LogUI.info("Class Name : " + className + "-Executing Method : " + methodName);
    }

    public Actions getActions() {
        return getActionsSeleniumFunctions().getActions();
    }

    public WebDriverWait getWait() {
        return getWaitingSeleniumFunctions().getWait();
    }

    public JavascriptExecutor getJavascriptExecutor() {
        return getJavaScriptExecutorSeleniumFunctions().getJavascriptExecutor();
    }

}
