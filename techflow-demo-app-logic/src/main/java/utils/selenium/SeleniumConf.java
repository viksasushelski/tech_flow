package utils.selenium;

import UiFunctions.Configuration.SeleniumConfiguration;
import org.openqa.selenium.WebDriver;

public class SeleniumConf {

    public static SeleniumConfiguration createStandardSeleniumConfiguration(WebDriver driver) {
        return SeleniumConfiguration.builder()
                .driver(driver)
                .defaultWaitingTime(30)
                .waitLocating(10)
                .indicatorWait(2)
                .build();
    }

}
