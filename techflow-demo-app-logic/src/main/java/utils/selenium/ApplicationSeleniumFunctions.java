package utils.selenium;

import UiFunctions.SeleniumFunctions;
import lombok.Getter;

@Getter
public class ApplicationSeleniumFunctions {

    private SeleniumFunctions seleniumFunctions;

    public ApplicationSeleniumFunctions(SeleniumFunctions seleniumFunctions) {
        this.seleniumFunctions = seleniumFunctions;
    }

}
