package data.enums;

import Txt.DataUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum ApplicationProperties {
    TEST_CONFIGURATION_PROPERTIES_FILE_PATH("tests_configuration.properties"),
    RESULTS_PROPERTIES_FILE_PATH("results.properties"),
    RESULTS_TEMPLATE_PROPERTIES_FILE_PATH("results-template.properties"),
    ENVIRONMENT_TEMPLATE_PROPERTIES_FILE_PATH("environment-template.properties"),
    ENVIRONMENT_PROPERTIES_FILE_PATH("allure-results/environment.properties"),
    BASE_URL_PROPERTY_NAME("baseUrl"),
    ENVIRONMENT_PROPERTY_NAME("environment"),
    THREAD_POOL_SIZE_PROPERTY_NAME("number.threads.per.suite"),
    HEADLESS_PROPERTY_NAME("headless"),
    DRIVER_VERSION_PROPERTY_NAME("driver.version"),
    DRIVER_TYPE("driver.type"),
    RERUN_NUMBER_OF_TIMES("re.run.failed.tests.count"),
    PASSED_TESTS_METHODS_PERCENTAGE("passed.tests.methods.percentage"),
    PASSED_TESTS_CLASSES_PERCENTAGE("passed.tests.classes.percentage"),
    PASSED_TESTS("tests.passed"),
    EXECUTION_PASSED_TESTS_METHODS_PERCENTAGE("execution.passed.tests.methods.percentage"),
    EXECUTION_PASSED_TESTS_CLASSES_PERCENTAGE("execution.passed.tests.classes.percentage"),
    INCLUDE_DATABASE_TESTING("include.database.testing"),
    DATABASE_QUERIES_THRESHOLD("database.queries.threshold"),
    PG_STATS_DB_IDENTIFIER("pg.stats.db.identifier"),
    ENABLE_CONSOLE_LOG("enable.console.log");

    private final String text;

    ApplicationProperties(final String text) {
        this.text = text;
    }

    private static Map<String, ApplicationProperties> values = populateMap();

    private static Map<String, ApplicationProperties> populateMap() {
        Map<String, ApplicationProperties> valuesMap = new HashMap<>();
        ApplicationProperties[] values = values();
        Stream.of(values).forEach(x -> valuesMap.put(x.text, x));
        return valuesMap;
    }

    @Override
    public String toString() {
        return text;
    }

    public static ApplicationProperties parse(String text) {
        if (!values.containsKey(text)) {
            throw new IllegalArgumentException("text");
        }

        return values.get(text);
    }

    public String getConfigurationValue() {
        return DataUtil.readFromProperties(ApplicationProperties.TEST_CONFIGURATION_PROPERTIES_FILE_PATH.toString(), this.toString());
    }
}
