package data.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum AccountType {

    SAVINGS("Savings"),
    CHECKING("Checking");

    private final String text;

    private AccountType(final String text) {
        this.text = text;
    }

    private static Map<String, AccountType> values = populateMap();

    private static Map<String, AccountType> populateMap() {
        Map<String, AccountType> valuesMap = new HashMap<>();
        AccountType[] values = values();
        Stream.of(values).forEach(x -> valuesMap.put(x.text, x));
        return valuesMap;
    }

    @Override
    public String toString() {
        return text;
    }

    public static AccountType parse(String text) {
        if (!values.containsKey(text)) {
            throw new IllegalArgumentException("text");
        }

        return values.get(text);
    }

}
