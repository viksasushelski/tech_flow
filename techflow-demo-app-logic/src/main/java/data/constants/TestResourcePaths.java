package data.constants;

import java.io.File;

/**
 * Created by vsushelski on 8/22/2017.
 */
public class TestResourcePaths {

    /**
     * Test data resources paths
     */
    public static final String STEPS_TO_REPRODUCE_PATH = "src" + File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar + "testStepsToReproduce" + File.separatorChar;
    public static final String CONSOLE_LOG_PATH = "src" + File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar + "testConsoleLog" + File.separatorChar;

    /**
     * Project folders resources paths
     */
    public static final String SUREFIRE_REPORTS_FOLDER_PATH = "target/surefire-reports/";
    public static final String TEST_OUTPUT_FOLDER_PATH = "test-output/";
}
