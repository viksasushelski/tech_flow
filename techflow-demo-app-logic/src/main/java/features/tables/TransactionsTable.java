package features.tables;

import Features.Tables.BaseTableReader;
import UiFunctions.Configuration.SeleniumConfiguration;
import models.Transaction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.selenium.SeleniumConf;

import java.util.Optional;

public class TransactionsTable extends BaseTableReader<Transaction> {

    public TransactionsTable(WebDriver driver) {
        super(SeleniumConf.createStandardSeleniumConfiguration(driver), By.className("table-condensed"), By.tagName("tbody"), By.tagName("tr"), null);
    }

    @Override
    public Transaction readTableRow(WebElement tableRow, WebElement header) {
        Optional<WebElement> dateOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(1)"));
        Optional<WebElement> descriptionOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(2)"));
        Optional<WebElement> depositOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(3)"));
        Optional<WebElement> withdrawalOptional = findTableColumn(tableRow, By.cssSelector("td:nth-child(4)"));
        return Transaction.builder()
                .date(dateOptional.map(this::readTableCellText).orElse(null))
                .description(descriptionOptional.map(this::readTableCellText).orElse(null))
                .deposit(depositOptional.map(this::readTableCellText).orElse(null))
                .withdrawal(withdrawalOptional.map(this::readTableCellText).orElse(null))
                .build();
    }
}
