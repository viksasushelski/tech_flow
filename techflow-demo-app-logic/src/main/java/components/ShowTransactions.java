package components;

import Components.Tables.BaseTableComponent;
import Features.Tables.BaseTableReader;
import features.tables.TransactionsTable;
import models.Transaction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShowTransactions extends BaseTableComponent<Transaction> {

    public ShowTransactions(WebDriver driver, WebElement tableContainer) {
        super(tableContainer, new TransactionsTable(driver));
    }
}
