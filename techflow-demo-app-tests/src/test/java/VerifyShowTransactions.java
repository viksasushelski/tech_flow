import Json.JsonParser;
import Log.LogUI;
import Models.AssertionResult;
import Utils.TestAssertionUtil;
import commons.results.ResultGeneration;
import data.enums.AccountType;
import io.qameta.allure.*;
import models.Transaction;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AccountActivityPage;
import pages.LoginPage;
import utils.base.BaseTest;

import java.util.List;

//Test Executor implementation from test-configuration module
@Epic("Smoke Tests")
@Feature("Transactions Tests")
public class VerifyShowTransactions extends BaseTest {

    public static final String EXPECTED_RESULTS_FILE_PATH = "src/test/resources/testData/expectedTransactions.json";
    private LoginPage loginPage;
    private AccountActivityPage accountActivityPage;

    @Override
    public void beforeClass() {
        loginPage = (LoginPage) new LoginPage(getDriver()).navigateTo(baseUrl + "/login.html");
        accountActivityPage = loginPage.loginWithCredentials("username", "password")
                .navigateToAccountActivityPage();
    }

    @Test(priority = 1, description = "Validate transactions are correct", groups = "blocker")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Validate when navigating to account activity page and choosing account type correct transactions values are displayed in the table")
    public void verifyCheckingTransactions() {
        accountActivityPage = accountActivityPage.chooseAccount(AccountType.CHECKING)
                .readTransactionTable();
        List<Transaction> actualTransactions = accountActivityPage.getShowTransactions().getListOfEntries();
        //Data reading implementation from data-reader module
        List<Transaction> expectedTransactions = JsonParser.parseJsonFromFileToListOfObject(EXPECTED_RESULTS_FILE_PATH, Transaction.class);
        //Collection assertion from assertions module
        AssertionResult assertionResult = TestAssertionUtil.checkIfListsAreEqual(actualTransactions, expectedTransactions);
        LogUI.assertion("Checking Transactions should look like [checkingTransactions]");
        ResultGeneration.addFileAsAttachment(EXPECTED_RESULTS_FILE_PATH, "checkingTransactions", "text/plain", ".txt");
        Assert.assertTrue(assertionResult.isPassed(), assertionResult.getErrorMessage());
    }
}
